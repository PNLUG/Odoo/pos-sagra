# Configurazione hardware

## SCENARIO MINIMALE:
* __cassa:__ 1 postazioni tipo [A]
* __bar:__ 1 postazione tipo [A]
* __cucina:__ 1 postazione tipo [A]

## SCENARIO SUGGERITO:
* __cassa:__ 2 postazioni tipo [A]
* __bar:__ 1 postazione tipo [A]
* __cucina:__ 1 postazione tipo [A]
* __vista fornelli:__ 1 postazione tipo [B]

## OPZIONE PRO
* __chiosco touch screen:__ postazione tipo [C]
* __server dedicato:__ postazione tipo [D]

# Tipologia hardware
### Tipo [A] da €560 a postazione
* PC:
    * notebook / PC / all in one
    * meglio se touch screen, in alternativa con mouse
    * minimo 2gb ram
    * qualsiasi sistema operativo
* Stampante con raspberry

### Tipo [B] da €200 a postazione
* monitor visibile
* rasberry

### Tipo [C] da €500 a postazione
* all in one touch screen

### Tipo [D] da €400 a postazione
* mini server HP

### Riferimenti ai costi orientativi:
* PC notebook € 400
* Stampante con raspberry € 85+€75
* monitor € 110
* postazione allinone € 450
* microserver G8/G10 hp € 400 con 3 dischi

Links: