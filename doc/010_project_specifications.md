# Configurazione sistema
Si dovrà ottenere una APP del nome pos_sagra che predisporrà ODOO per la gestione di una sagra con il seguente flusso di lavoro:

# FASE 1 INSERIMENTO ORDINE:
## Da interfaccia POS:
* inserimento ordine
* stampa ricevuta per il cliente (due copie in fila) 
* se per asporto stampare: "PRESENTARSI AL PUNTO DI ASPORTO"
* campo note: dev'essere una nota unica per tutto l'ordine 

* aggiungere tempo attesa configurabile tra cassa-bar e bar-cucina
* gestione cambio stato con log di attività
* visualizzazione pivot (ordine/prodotti) in cucina/bar
* anteprima, stampa, conferma e evasione singolo ordine 

* visualizzazione pivot per tutti i prodotti in portafoglio

## Da TOTEM/CASA/SMARTPHONE :
* se attivato il flag asporto:
diventa obbligatorio il campo nome
diventa selezionabili solo i prodotti di cucina
stampare in chiaro il nome per l'asporto
inibire l'uso del pulsante guest sulla gui POS
inibire il pulsante trasferimento sulla gui POS

####  Comportamento solo da TOTEM
 * la scelta del numero di tavolo dev'essere obbligatoria
 * stampare in chiaro il numero di tavolo sotto QR code

#### Comportamento solo da CASA
 * la scelta del numero di tavolo non compare
 * stampare "INFORMATI DEL NUMERO DI TAVOLO PRIMA DI PRESENTARTI IN CASSA"

* stampare promemoria dei prodotti 
* stampare il QRC
# FASE 2 GESTIONE ORDINE:
superata la fase 1, l'ordine verrà visualizzato nelle viste preposte:
* Vista BAR/CUCINA
* Vista FORNELLI

Ci sarà un tempo impostabile per consentire eventuali variazioni dell'ordine.
Cosa succede durante questo intervallo?
* sarà resa impossibile l'evasione dell'ordine.
* si portrà accedere dall'intefaccia POS e riaprire l'ordine per la modifica
* 

## Vista BAR/CUCINA
Sono due viste equivalenti, una per le bibite e l'altra per la cucina, selezionabile dal browser.
La vista consiste in una tabella popolata con i prodotti e gli ordini da evadere in sequenza cronologica. Inoltre in testa alle colonne, ci saranno i totali per ogni prodotto.
Per ogni riga (ordine) ci sarà modo di procedere alla stampa dell'ordine e conseguente evasione dall'elenco (previa verifica stampa corretta)

## Vista FORNELLI
E' una tabella configurabile dall'utente dove ci saranno i totali di prodotti scelti ancora da evadere, e se attivata l'apposita opzione la giacenza in tempo reale.
Ci potranno definire più viste fornelli richiamabili dal browser


# OPZIONI DEL PROGRAMMA
Sono da predisporre le seguenti configurazioni impostabili dall'utente:
* [SI/NO] totale automatico su pagamento (serve per l'eventualità di 2 operatori, uno al PC altro alla cassa)
* [##]    minuti di ritardo abilitazione ordine cucina dopo stampa bar
* [SINGOLO/DOPPIO] stampa scontrino singolo / doppia copia
* [SI/NO] opzione pre-taglio scontrino su cambio categoria prodotto
* [SI/NO] opzione stampa si/no orario stampa e medie consegna
* [##] sequenza di visualizzazione prodotti

# Stati possibili della testata degli ordini

| Stato | Significato                |
|:-----:|:---------------------------|
|   I   | Inserito in cassa (pagato) |
|   B   | Evaso bar                  |
|   E   | Evaso totale               |
|   D   | Annullato                  |
|   X   | Asporto                    |

# Flusso di relazione logica ←-→ stato ordine ammessi (AFFERENZA)

|settore |Righe da visualizzare con stato         |Impostazione stato ordine in uscita|
|:------:|:---------------------------------------|:----------------------------------|
| CASSA  |I – X (entro 3 minuti dall’inserimento) |I - X                              |
| BAR    |I                                       |B (E)                              |
| CUCINA |B - X                                   |E                                  |



## Futuro:
* gestire i tempi di evasione
 * sul piè di pagina aggiungere:
 * in cassa:   inserito da .... alle .... / medie di evasione
 * in bar:     inserito in cassa da .... evaso bar da .... alle ... / medie di evasione
 * in cucina:  inserito in cassa da .... evaso bar da .... alle ... evaso cucina da .... alle ... / medie di evasione
 
* customizzare interfaccia utente togliendo le voci superfule
* opzione di gestione si/no magazzino
* interfaccia per modificare l'ordinamento dei prodotti
* impostare timer per l'evasione dell'impegnato a video
* oppure gestire l'evasione con un'ulteriore passaggio
* gestire i tempi di evasione
* pagare il caffè insieme con l'ordine principale

### Da verificare come impostare la BOM
* gestione distinta per piatto: es. fette di polenta
* cassa bar con alcuni prodotti cucina: es. patatine. Vanno considerate nel totale da fare ma NON mostrati nell'elenco comande cucina.
