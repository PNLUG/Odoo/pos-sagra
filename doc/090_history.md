# **Attività svolte**


#### 28/06/18->08/07/18
**Sagra - Francenigo**
 
|Componente | Qta |
|-----------|-----|
| Server    | ?   |
| Casse     | 2   |
| Cucina    | 1   |
| Bar       | 1   |
| Fornelli  | 1   |

***
### 29/06/18->01/07/18
***Sagra - Cimpello***
|Componente | Qta |
|-----------|-----|
| Server    | ?   |
| Casse     | 2   |
| Cucina    | 1   |
| Bar       | 1   |
| Fornelli  | 1   |
| Totem     | 1   | 
Note: funzione totem su tablet con hostes

***
### ??/??/18->??/??/18
***Sagra San Piero - Cordenons***
|Componente | Qta |
|-----------|-----|
| Server    | 1   |
| Casse     | 2   |
| Cucina    | 1   |
| Bar       | 1   |
| Fornelli  | 1   |
| Totem     | 2   | 

***
### 09/11/18->11/11/18
***Sagra San Martino - Cordenons***
|Componente | Qta |
|-----------|-----|
| Server    | 1   |
| Casse     | 2   |
| Cucina    | 1   |
| Bar       | 1   |
| Fornelli  | 1   |
| Totem     | 1   |

***
### 09/11/18->18/11/18
***Sagra Del Porzel - Pasiano***
|Componente | Qta |
|-----------|-----|
| Casse+Srv | 1   |
