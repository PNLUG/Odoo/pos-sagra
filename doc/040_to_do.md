
- Priorità 1
    - [ ] sistemare scontrino (tagli, ordine prodotti)
    - [ ] sistemare comande (data-ora)
    - [ ] riscrivere vista bar/cucina
    - [ ] associare al POS lo stato-ordine visibile e le categorie prodotti visibili
    - [ ] ? associare la tipologia order/service alle singole stampanti
    - [ ] creare vista fornelli
    - [ ] Ereditare il campo/pulsante NOTE del POS, e sostituirlo con NOTA ORDINE invece che la nota per prodotto
    - [x] Ereditare il pulsante FATTURA e sostituire ANTEPRIMA SCONTRINO
    - [x] togliere il pulsante STAMPA e OK dall'anteprima scontrino
    - [x] Ereditare il pulsante Ordine e spostare l'azione dopo la validazione del pagamento
    - [x] Ereditare i pulsanti Cliente,Dividi, Sconto, Prezzo, +/- e nasconderli
    - [ ] Cambiare il testo sul pulsante GUEST con COPERTI
  - Quando è attivo l'ASPORTO
    - [ ] inibire l'uso del pulsante guest
    - [ ] inibire il pulsante trasferimento
    - [ ] inibire il pulsante **categorie bibite**

- Priorità 2
    - [ ] creare vista ordini totale (con ristampa?)
    
- Aggiunte
    - [ ] gestione smartphone
    - [ ] gestione web
