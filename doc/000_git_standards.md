# Standard del progetto / Project standards
---
# Italiano
## GIT
Prefissi da utilizzare nei commit (rif. https://www.odoo.com/documentation/8.0/reference/guidelines.html):

    [IMP] per i miglioramenti
    [FIX] per la risoluzione di problemi
    [REF] per il refactoring
    [ADD] per l'implementazione di nuove funzionalità
    [REM] per l'eliminazione di funzionalità
    [DOC] per interventi di documentazione
    [TST] per prove e test
    [MERGE] per l'unione di branch diversi (only for forward/back-port)

Nomi dei brach:
- **master**: branch standard di riferimento con le relase stabili
- **relase**: branch standard per le relase beta e relativi fix
- **dev**: branch standard di sviluppo
- **USR_TYP_DESC**: branch degli sviluppatori dedicati a specifiche attività
    -> USR: iniziali nome e congnome dello svilupatore
    -> TYP: tipo di branch: usare prefissi standard dei commit
    -> DESC: nome del branch
- **cstm_CUSTOMER_TYP**: branch per cliente
    -> CUSTOMER: stringa senza spazi che identifica il cliente
    -> TYP: classificazione branch base (master/dev/relase ...)
&nbsp;
---
# English

## GIT
Prefix your commit with (ref. https://www.odoo.com/documentation/8.0/reference/guidelines.html)

    [IMP] for improvements
    [FIX] for bug fixes
    [REF] for refactoring
    [ADD] for adding new resources
    [REM] for removing of resources
    [TST] for testing
    [MERGE] for merge commits (only for forward/back-port)

Branch's names:
- **master**: standard branch with stable relases
- **relase**: standard branch for beta relases and relative fixes
- **dev**: standard branch for development
- **USR_TYP_DESC**: developers's branches used for specific activities
    -> USR: developer's first letter of name and surname
    -> TYP: type of branch: use commit standard prefix
    -> DESC: name of the branch
- **cstm_CUSTOMER_TYP**: client's branch
    -> CUSTOMER: string without spaces that identifies the customer
    -> TYP: base branch classification (master/dev/relase ...)
